
# Tile
<p align="center">
  <a href="https://github.com/godcheese/tile">
    <img src="https://img.shields.io/github/last-commit/godcheese/tile.svg" alt="GitHub Last Commit">
  </a>
  </a>
  <a href="https://travis-ci.org/godcheese/tile" rel="nofollow">
    <img src="https://travis-ci.org/godcheese/tile.svg?branch=master" alt="Build Status">
  </a>
   <a href="https://sonarcloud.io/dashboard?id=godcheese_tile"><img src="https://sonarcloud.io/api/project_badges/measure?project=godcheese_tile&metric=alert_status" alt="Quality Gate Status"/></a>
  <a href="https://www.codacy.com/app/godcheese/tile?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=godcheese/tile&amp;utm_campaign=Badge_Grade"><img src="https://api.codacy.com/project/badge/Grade/eadbab0985f44b5e98608d644a609dc4" alt="Build Status"/></a>
  <a href="https://github.com/godcheese/tile/blob/master/LICENSE">
    <img src="https://img.shields.io/github/license/mashape/apistatus.svg" alt="license">
  </a>
</p>

## 简介 Introduction

Tile 是一款自用的 Java 快速开发工具集依赖包，提供一些常用的实用类。

## 特性 Features

 - DatabaseUtil
   - [SQL 文件生成实体类、MyBatis Mapper、MyBatis Mapper XML 文件](/docs/DatabaseUtil.md)


## Install
- Maven
  - ``` mvn clean install ```

## Usage
- Maven：将 tile-1.0.0.jar 拷贝只项目根目录下的 lib 目录下，然后再 pom.xml 文件下添加以下依赖代码：
```
<dependency>
    <groupId>com.gioov</groupId>
    <artifactId>tile</artifactId>
    <version>1.0.0</version>
    <scope>system</scope>
    <systemPath>${project.basedir}/lib/tile-1.0.0.jar</systemPath>
    <type>jar</type>
</dependency>
```

## 捐赠 Donation

If you find Tile useful, you can buy us a cup of coffee

[Paypal Me](https://www.paypal.me/godcheese)
