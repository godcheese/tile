#!/usr/bin/env bash
echo "author godcheese"
CURRENT_DIR=$(pwd)
SCRIPTS_DIR=$(cd "$(dirname $0)" || exit; pwd)
cd "${SCRIPTS_DIR}" || exit
cd ..
git clone https://github.com/godcheese/maven-repository.git
mvn clean deploy -Dmaven.test.skip -DaltDeploymentRepository="github-mvn-repo::default::file:`pwd`/maven-repository/repository"
cd maven-repository
echo "Add file..."
git add -A
git commit -m "deploy release package."
echo "Submit code..."
git push origin master
echo "Submit complete,close..."
cd "${CURRENT_DIR}"
rm -rf maven-repository