package com.gioov.tile.crypto;

import java.security.MessageDigest;

/**
 * @author godcheese [godcheese@outlook.com]
 * @date 2018-02-07
 */
public class Md5Util {

    private final static String[] HEX_DIGITS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    /**
     * 转换byte到16进制
     *
     * @param b 要转换的byte
     * @return 16进制对应的字符
     */
    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n = 256 + n;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return HEX_DIGITS[d1] + HEX_DIGITS[d2];
    }

    /**
     * @param bytes 字节数组
     * @return
     */
    private static String byteArrayToHexString(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes) {
            stringBuilder.append(byteToHexString(b));
        }
        return stringBuilder.toString();
    }

    public static String encrypt(String plaintext) {
        String result = null;
        try {
            MessageDigest messageDIgest = MessageDigest.getInstance("MD5");
            messageDIgest.update(plaintext.getBytes());
            result = byteArrayToHexString(messageDIgest.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    public static String encrypt(String plaintext, String charset) {
        String result = null;
        try {
            MessageDigest messageDIgest = MessageDigest.getInstance("MD5");
            messageDIgest.update(plaintext.getBytes(charset));
            result = byteArrayToHexString(messageDIgest.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

}
