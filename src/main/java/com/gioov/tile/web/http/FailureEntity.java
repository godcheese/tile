package com.gioov.tile.web.http;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gioov.tile.web.exception.BaseResponseException;
import org.springframework.http.HttpStatus;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author godcheese [godcheese@outlook.com]
 * @date 2018-05-29
 */
public class FailureEntity {
    private String message;
    private int code = 0;
    private long timestamp = Instant.now().toEpochMilli();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public FailureEntity() {
    }

    public FailureEntity(FailureMessage failureMessage) {
        this.message = failureMessage.getMessage();
        this.code = failureMessage.getCode();
        this.timestamp = failureMessage.getCode();
    }

    public FailureEntity(BaseResponseException baseResponseException) {
        this.message = baseResponseException.getMessage();
        this.code = baseResponseException.getCode();
        this.timestamp = baseResponseException.getTimestamp();
    }

    public FailureEntity(HttpStatus httpStatus) {
        this.message = httpStatus.getReasonPhrase();
        this.code = httpStatus.value();
    }

    public FailureEntity(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public FailureEntity(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = new LinkedHashMap<>(3);
        map.put("message", message);
        map.put("code", code);
        map.put("timestamp", timestamp);
        try {
            return objectMapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

}