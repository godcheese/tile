package com.gioov.tile.web.http;

/**
 * @author godcheese [godcheese@outlook.com]
 * @date 2018-05-29
 */
public interface FailureMessage {

    /**
     * message
     * @return
     */
    String getMessage();

    /**
     * code
     * @return
     */
    int getCode();

    long getTimestamp();

}