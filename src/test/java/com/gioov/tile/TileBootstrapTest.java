package com.gioov.tile;

import com.gioov.tile.util.ClientUtil;
import com.gioov.tile.util.RandomUtil;
import com.gioov.tile.web.exception.BaseResponseException;
import com.gioov.tile.web.http.FailureMessage;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.net.UnknownHostException;


/**
 * Unit test for simple App.
 */
public class TileBootstrapTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TileBootstrapTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TileBootstrapTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testTileBootstrap() {

//        try {
//            System.out.println("9"+ ClientUtil.getLocalHostLANAddress());
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }

//        // 指定单个 SQL 文件生成对应的实体类、Mybatis Mapper、MyBatis Mapper XML 文件
//        SqlGenerateProperties properties = new SqlGenerateProperties();
//        properties.setMyBatisMapperPackage("com.gioov.demo.mapper");
//        properties.setEntityPackage("com.gioov.demo.entity");
//
//        DatabaseUtil.generateEntity("D:\\demo\\tables\\api_table.sql","D:\\demo\\src\\main\\java\\com\\gioov\\demo\\entity", properties);
//        DatabaseUtil.generateMyBatisMapper("D:\\demo\\tables\\api_table.sql","D:\\demo\\src\\main\\java\\com\\gioov\\demo\\mapper", properties);
//        DatabaseUtil.generateMyBatisMapperXml("D:\\demo\\tables\\api_table.sql","D:\\demo\\src\\main\\java\\com\\gioov\\demo\\mapper", properties);


//        // 批量生成实体类、Mybatis Mapper、MyBatis Mapper XML 文件
//        SqlGenerateProperties properties = new SqlGenerateProperties();
//        properties.setSqlFileSuffix("_table.sql");
//        properties.setMyBatisMapperPackage("com.gioov.demo.mapper");
//        properties.setEntityPackage("com.gioov.demo.entity");
//
//
//        properties.setSqlDirectory("D:\\tables");
//        properties.setEntityDirectory("D:\\demo\\src\\main\\java\\com\\gioov\\demo\\entity");
//        properties.setMyBatisMapperDirectory("D:\\demo\\src\\main\\java\\com\\gioov\\demo\\mapper");
//        properties.setMyBatisMapperXmlDirectory("D:\\demo\\src\\main\\java\\com\\gioov\\demo\\mapper");
//
//        DatabaseUtil.generateEntity(properties);
//        DatabaseUtil.generateMyBatisMapperXml(properties);
//        DatabaseUtil.generateMyBatisMapper(properties);

//        DataSizeUtil.Pretty pretty = DataSizeUtil.pretty(1501295805 , 1000);
//
//        System.out.println(pretty.getPrettySize());
//        System.out.println(pretty.getName());
//        System.out.println(pretty.getUnit());
//        BigDecimal bigDecimal = new BigDecimal(String.valueOf(pretty.getPrettySize()));
//        System.out.println(bigDecimal.setScale(2, RoundingMode.HALF_DOWN));

//        System.out.println(DataSizeUtil.prettySize(20431295805L, 1000));
//        System.out.println(DataSizeUtil.format(20431295805L, DataSizeUtil.SizeEnum.GIGA_BYTE, 1000));

//        System.out.println(DataSizeUtil.pretty(1559295805 , 1000).getPrettySize());

//        System.out.println(StandardCharsets.UTF_8.name());
    }

}

